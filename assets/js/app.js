require('jquery');
require('popper.js');
require("select2");

const imagesContext = require.context('../images', true, /\.(png|jpg|jpeg|gif|ico|svg|webp)$/);
imagesContext.keys().forEach(imagesContext);



const $ = require('jquery');
global.$ = global.jQuery = $;

require('../scss/bootstrap-italia-custom.scss');
require('bootstrap-italia');

import 'typeface-titillium-web';
import 'typeface-roboto-mono';
import 'typeface-lora';

import "@fortawesome/fontawesome-free/js/all";

const bootbox = require('bootbox');
global.bootbox =bootbox;

