<?php

namespace App\Command;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserCreateCommand extends Command
{
    protected static $defaultName = 'user:create';

    public function __construct(EntityManagerInterface $entityManager, UserPasswordEncoderInterface $passwordEncoder)
    {
        parent::__construct();
        $this->entityManager = $entityManager;
        $this->passwordEncoder = $passwordEncoder;
    }

    protected function configure()
    {
        $this
            ->setDescription('Create user');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        $io->title('User creation');
        $username = $io->ask('Username', null, function ($username) {
            if (empty($username)) {
                throw new \RuntimeException('Username cannot be empty.');
            }

            return $username;
        });
        $password = $io->askHidden('Password', function ($password) {
            if (empty($password)) {
                throw new \RuntimeException('Password cannot be empty.');
            }

            return $password;
        });

        $email = $io->ask('Email', null, function ($email) {
            if (empty($email)) {
                throw new \RuntimeException('Email cannot be empty.');
            }

            return $email;
        });
        $firstname = $io->ask('Nome', null, function ($firstname) {
            if (empty($firstname)) {
                throw new \RuntimeException('Nome cannot be empty.');
            }

            return $firstname;
        });
        $lastname = $io->ask('Cognome', null, function ($lastname) {
            if (empty($lastname)) {
                throw new \RuntimeException('Cognome cannot be empty.');
            }

            return $lastname;
        });
        $ruolo = $io->ask('Ruolo', null, function ($ruolo) {
            if (empty($ruolo)) {
                throw new \RuntimeException('Ruolo cannot be empty.');
            }

            return $ruolo;
        });
        $user = new User();
        $user->setUsername($username);
        $user->setEmail($email);
        $user->setPassword($this->passwordEncoder->encodePassword(
            $user, $password
        ));
        $user->setFirstname($firstname);
        $user->setLastname($lastname);
        $user->setRoles([
            strtoupper('ROLE_'.$ruolo)
        ]);
        $this->entityManager->persist($user);
        $this->entityManager->flush();

        $io->success('Utente '.$username.' creato.');

        return 0;
    }
}
