<?php

namespace App\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;

class DefaultController extends AbstractController
{
    /**
     * @Route("/", name="homepage")
     */
    public function homepage()
    {
        return new JsonResponse('ok');
    }


    /**
     * @Route("/cippo", name="cippo")
     */
    public function cippo()
    {

        return $this->render('default/cippo.html.twig', [
         'messaggio'=>'ciao'
        ]);
    }

}
